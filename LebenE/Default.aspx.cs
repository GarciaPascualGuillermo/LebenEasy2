﻿using System;
using System.ServiceModel;
using System.Web.UI;



namespace LebenE
{
    public partial class _Default : Page
    {
        //Esta funcion es la principal
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        //funcion con la cual se puede hacer el logueo
        public void Authentication(object sender, EventArgs e)
        {

            // Create the binding.
            BasicHttpsBinding myBinding = new BasicHttpsBinding(BasicHttpsSecurityMode.TransportWithMessageCredential);
            myBinding.MaxReceivedMessageSize = Int32.MaxValue;
            EndpointAddress ea = new
           // EndpointAddress("https://api.lebenws.com/2014/12/SOAP/Basic/");
           //EndpointAddress("https://api.brightsignnetwork.com/2014/12/SOAP/Basic/");
           EndpointAddress("https://api.lebencompany.com/2014/12/SOAP/Basic/");
            WS.ApplicationServiceClient cc = new WS.ApplicationServiceClient(myBinding, ea);
            cc.ClientCredentials.UserName.UserName = Request.Form["txt_user"];
            cc.ClientCredentials.UserName.Password = Request.Form["txt_pass"];
            String u = Request.Form["txt_user"];
            String p = Request.Form["txt_pass"];
            Boolean bandera = false;
            WS.User user = null;
            try
            {
                cc.Open();
                user = cc.Authenticate();
                bandera = true;
                cc.Close();
            }
            finally
            {
                // Response.Redirect("Default.aspx");
                if (bandera == true)
                {
                    Session["User"] = user;
                    Session["Usuario"] = u;
                    Session["Pass"] = p;
                    Session["List"] = null;
                    Session["Valor"] = null;
                    Response.Redirect("Grupos.aspx");

                }
                else
                {
                    Session["User"] = null;
                    Response.Redirect("Login.aspx");
                }

            }


        }


    }
}