﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="LebenE.Error" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="css/error.css">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            
            
            <div class="padre">
            <label runat="server" id="error_code" class="hijo_uno">¡ERROR 500!</label>
                <br />
            <label runat="server" id="ex_message" class="hijo_dos">Se ha producido un error inesperado en servidor.</label>
                <br />
                <br />
                <asp:Image src="Img\error.jpg" CssClass="hijo_imagen" runat="server"/>
                <br />

                <button id="come_back" runat="server" onServerClick="Home" class="button_regresar">Regresar</button>
                </div>

            

        </div>
    </form>
</body>
</html>
