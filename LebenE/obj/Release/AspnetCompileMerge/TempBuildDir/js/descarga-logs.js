function validarTabla() {
    var filas = $('#table_players').DataTable().rows({ selected: true }).data();
    if (filas.length == 0) {
        alert("Debe seleccionar al menos 1 Dispositivo");
        return false;
    } else {
        return true;
    }
}


function show_descargar_logs_modal() {
    if (validarTabla()) {
        document.getElementById("myModal-log").style.display = "block";
    }
}

function show_descargar_group_logs_modal() {
        document.getElementById("myModal-log").style.display = "block";
}  
  
  function hide_descargar_logs_modal(){
    document.getElementById("myModal-log").style.display = "none";
}
