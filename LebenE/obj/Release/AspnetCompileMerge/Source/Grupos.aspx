﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Grupos.aspx.cs" Inherits="LebenE.Grupos" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="widtd=device-widtd, initial-scale=1.0">

    <!-- Estilos css y estilos DataTables -->
    <link rel="stylesheet" href="css/nav-bar.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/general.css">
    <link rel="stylesheet" href="css/table-players.css">
    <link rel="stylesheet" href="css/descarga-logs.css">
    <link rel="stylesheet" href="css/modal-usuario.css">
    <link rel="stylesheet" href="css/DataTables.css">
    <link rel="stylesheet" href="css/DataTables.Select.min.css">
    <link rel="stylesheet" href="css/loading.css">

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">

    <title>Bienvenido a LebenEasyPlayer</title>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
</head>
<body class="background-dark">

    <form id="f_menuPage" runat="server">
    <!-- Barra de navegación -->
    <nav>
        <input type="checkbox" id="check">
        <label for="check" class="checkbtn">
        <i class="fas fa-bars"></i>
    </label>

        <img class="img-logo" src="Img/leben_simbol.png" alt="">
        <label class="logo">
        LebenEasyPlayer</label>
        <ul>
            <li><asp:LinkButton runat="server" CssClass="hyperlink" NavigateUrl="#" ID="home" OnClick="Home" Text="Inicio"/></li>
            <!-- Codigo para ejecutar funciones Javascript desde la propiedad href de los elementos 'hyperlink' -->
            <li><asp:HyperLink runat="server" CssClass="hyperlink" NavigateUrl="javascript:show_propiedadesUser();" ID="user_prop_hyper" Text="Mi perfil"/></li>
            <!-- //Fin de código -->
            <li><asp:LinkButton runat="server" CssClass="hyperlink active" NavigateUrl="#" ID="admin_grupos_hyper" Text="Reporte por grupos"/></li>
            <li><asp:LinkButton ID="link_button" CssClass="hyperlink" runat="server" OnClick="Logout" Text="Cerrar Sesión" /></li>
        </ul>
    </nav>

     <!-- Botón para generar reportes -->
    <div class="col-buttons">
       
        <label id="modal_report" class="button-inverse" onclick="alert('Error. Debe  seleccionar al menos 1 Dispositivo.');" runat="server">Generar Reporte</label>
        <button id="SelectAll" class="button-inverse" onServerClick="PressButtonAll" onclick="show_container_loading();" runat="server">Seleccionar Todo</button>

        </div>
        <br />


         <div class="table-container2">
        <!-- Tabla para players -->
         <asp:PlaceHolder id="tabla" runat="server"></asp:PlaceHolder>
         
            <!-- <asp:Label ID="label1" runat="server" Text="Hola"></asp:Label>
            
             <asp:Label runat="server" ID="Label9">sdgsd</asp:Label>
             <asp:Label runat="server" ID="Label2">sdgsd</asp:Label> 
              -->
        
        <!-- Card -->
            </div>
   

<!-- CODIGO PARA DESCARGAR EL LOG ESPECIFICADO -->
    <!-- The Modal-log -->
    <div id="myModal-log" class="modal-log">

        <!-- Modal-log content -->
        <div class="modal-log-content">
            <div class="modal-log-header">
                <label id="myClose-log" class="close-log" onclick="hide_descargar_logs_modal();"> &times; </label>
            </div>
            <div class="modal-log-body">
                <h2>Propiedades de su archivo log</h2>
                <br>
                
                    <b>Seleccione su tipo de Reporte</b>
                    <br>
                    <p><asp:DropDownList ID="report_period" CssClass="dropdownlist-dark pickers" runat="server"></asp:DropDownList></p><input type="date" runat="server" name="inicio_fecha" id="dt_inicio" class="pickers">
                    <br>
                    <br>
                    <b>Seleccione su formato del archivo log</b>
                    <br>
                    <br>
                    <input type="radio" runat="server" name="rd_formato" id="rd_DB"  value="database" checked/><label for="rd_DB">Base de Datos</label>
                    <input type="radio" runat="server" name="rd_formato" id="rd_json" value="json" class="checkers" /> <label for="rd_json">JSON(.json)</label>
                    <input type="radio" runat="server" name="rd_formato" id="rd_pdf" value="pdf" class="checkers"/><label for="rd_pdf">PDF(.pdf)</label>
                    <input type="radio" runat="server" name="rd_formato" id="rd_csv" value="csv" class="checkers"/><label for="rd_csv">Excel(.csv)</label>
                    <!-- <input type="radio" name="rd-formato" id="rd-xlsx" value="xlsx" ><label for="rd-xlsx">Excel(.xlsx)</label></input> -->
                    <br>
                    <br>
                    <br>
                <button name="download" id="HTMLbtn_downloadLog" class="button-inverse" runat="server" onserverclick="getReports" onclick="show_container_loading();">Descargar log</button>
                
            </div>
        </div>

    </div>
    <!-- //FIN DE CODIGO PARA DESCARGAR EL LOG ESPECIFICADO -->


    <!-- MODAL PARA VER LAS PROPIEDADES DE LA SESION -->

    <div class="modal-user" id="modal_propiedades_user" runat="server">
        <div class="modal-user-content">
            <div class="modal-user-body">
                <h2>Propiedades del Usuario</h2>
                    <br />
                    <p><b>User ID:</b> <asp:Label runat="server" ID="lb_userID">sdgsd</asp:Label></p>
                    <br />
                    <p><b>User Name:</b> <asp:Label runat="server" ID="lb_username">sdgsd</asp:Label></p>
                    <br />
                    <p><b>User LastName:</b> <asp:Label runat="server" ID="lb_userlastname">sdgsd</asp:Label></p>
                    <br />  
                    <p><b>User Email:</b> <asp:Label runat="server" ID="lb_userEmail">sdgsd</asp:Label></p>
                    <br />
                    <p><b>Role:</b> <asp:Label runat="server" ID="lb_role">sdgsd</asp:Label></p>
                    <br />
                    <p><b>Descripción:</b> <asp:Label runat="server" ID="lb_descrip">sdgsd</asp:Label></p>
                    <br />
                    <button class="button-inverse" id="x" onclick="hide_propiedadesUser();">Aceptar</button>
            </div>
        </div>
         
    </div>

   
    <!-- //FIN DE MODAL PARA VER LAS PROPIEDADES DE LA SESION -->

        <div id="container_loader" runat="server">
        <div id="load"></div>
        <label id="legend">Procesando la petición . . .</label>
    </div>

</form>
</body>

    <script type="text/javascript" src="js/descarga-logs.js"></script>
    <script type="text/javascript" src="js/modal-usuario.js"></script>
    <script type="text/javascript" src="js/loading.js"></script>

</html>
