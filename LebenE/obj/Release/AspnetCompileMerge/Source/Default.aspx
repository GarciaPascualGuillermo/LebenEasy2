﻿<%@ Page Title="Home Page" Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="LebenE._Default" %>


<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
  <!-- Required meta tags -->
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="css/login.css">
  <title>Bienvenido a PlayManager by Leben Development</title>
</head>

<body>
    <div class="container">
      <div class="card-container">
        <div class="header">
          <div><img width="90%" class="boardroom" src="Img/boardroom.png" alt=""></div>
          </div>
        <div class="formulario">
          <img width="65%" src="Img/leben_logo-inverse-273x155px.png" alt="">
          <br/>
            <form id="form1" runat="server">  
          <br/>
          <br/>
           <input id="txt_user" runat="server" class="entrada_text" type="text" placeholder="Nombre de usuario"/>
          <br/>
          <input id="txt_pass" runat="server" class="entrada_text" type="password" placeholder="Contraseña"/>
          <br/>
          <asp:Button ID="Button1" CssClass="button_ingresar"  onclick="Authentication" runat="server"  Text="Ingresa" />
         </form>
          <br/>
          <label>¿No tienes una cuenta?</label>
          <a class="link" href="#">Contáctanos.</a>
          <br/>
          <br/>
          <a class="link" href="#">¿Olvidaste tu contraseña?</a>
        </div>
      </div>
    </div>
</body>

<script src="js/login.js">
    document.getElementById("btn_ingresar").click()

</script>

    <script >

   //resto del script si lo hay

  @if(ViewBag.Message != ""){

      alert('@ViewBag.Message');

   }

    </script>

</html>