// Funcion para ABRIR el grupo seleccionado...
function showGroup(idGrupo,e) {
    if (e.innerText == '+') {

        if (document.getElementsByClassName("showing").length == 0) {
            $("div.dashboard-cards")
                .addClass("showing");
        }

       

        $("#" + idGrupo)
            .css({ zIndex: 5 })
            .addClass("d-card-show");
        e.innerText = '-';
        e.style.background = '#ff3a3a';
    }
    else {

        $("#" + idGrupo)
            .css({ zIndex: 2 })
            .removeClass("d-card-show");

        e.innerText = '+';
        e.style.background = '#828282';

        if (document.getElementsByClassName("d-card-show").length == 0) {
            $("div.dashboard-cards")
                .removeClass("showing");
        }
    }
}


// Funcion para CERRAR el grupo seleccionado...
function closeGroup(idGrupo) {
    $("#" + idGrupo)
        .css({ zIndex: 2 })
        .removeClass("d-card-show");

    document.getElementById(idGrupo).children[0].children[1].innerText = '+';
    document.getElementById(idGrupo).children[0].children[1].style.background = '#828282';

    if (document.getElementsByClassName("d-card-show").length == 0) {
        $("div.dashboard-cards")
            .removeClass("showing");
    }
}


//Funciones para asignar atributo 'Checked' a checkbox de un card...
function select_group(checkbox_id,elemnt) {

    if (document.getElementById(checkbox_id).checked == false) {

        document.getElementById(checkbox_id).checked = true;
        elemnt.classList.add("active");
        var cb_devices = document.getElementsByClassName("g-" + checkbox_id);
        for (var i = 0; i < cb_devices.length; i++) { cb_devices[i].checked = true; cb_devices[i].parentNode.parentNode.style.background = "#ff3a3a"; }


    } else {
        document.getElementById(checkbox_id).checked = false;
        elemnt.classList.remove("active");
        var cb_devices = document.getElementsByClassName("g-" + checkbox_id);
        for (var i = 0; i < cb_devices.length; i++) { cb_devices[i].checked = false; cb_devices[i].parentNode.parentNode.style.background = "none";}


    }
}


function stateChanged(e) {
    e.classList.toggle("active");
}

function deviceStateChanged(e) {
    if (e.children.length > 0) {
    if (e.children[1].children[0].checked != true) { e.style.background = "#ff3a3a"; e.children[1].children[0].checked = true;}
        else { e.style.background = "none"; e.children[1].children[0].checked = false; }
    }
    onclickGroupStateChanged();
}


function select_all() {
    var grupos = document.getElementsByClassName("checkbox-grupal");
    for (var i = 0; i < grupos.length; i++) {
        grupos[i].checked = true;
        if (!grupos[i].parentElement.classList.contains("active")) { grupos[i].parentElement.classList.add("active"); }  
    }

    var devices = document.getElementsByClassName("checkbox-individual");

    for (var j = 0; j < devices.length; j++) {
        devices[j].checked = true;
        devices[j].parentNode.parentNode.style.background = "#ff3a3a";
    }
}

function deselect_all() {
    var grupos = document.getElementsByClassName("checkbox-grupal");
    for (var i = 0; i < grupos.length; i++) {
        grupos[i].checked = false;
        if (grupos[i].parentElement.classList.contains("active")) { grupos[i].parentElement.classList.remove("active"); }
    }

    var devices = document.getElementsByClassName("checkbox-individual");

    for (var j = 0; j < devices.length; j++) {
        devices[j].checked = false;
        devices[j].parentNode.parentNode.style.background = "none";
    }
}

function onclickGroupStateChanged(){
    var cards = document.getElementsByClassName("checkbox-grupal");

    for (var c = 0; c < cards.length; c++) {
        var devices = cards[c].parentElement.parentElement.parentElement.getElementsByClassName("checkbox-individual");

        cards[c].checked = false;
        if (devices.length > 0) {
            for (var d = 0; d < devices.length; d++) {
                if (devices[d].checked == true) {cards[c].checked = true; }
            }
            var label = cards[c].parentElement;
            if (cards[c].checked == true) { if (!label.classList.contains("active")) { label.classList.add("active"); } } else { if (label.classList.contains("active")) { label.classList.remove("active"); } }
        }

        }
}


//Funcion para cargar estilos de los grupos.
function onloadItemsStateChanged() {
    var cards = document.getElementsByClassName("checkbox-grupal");

    for (var c = 0; c < cards.length; c++) {
        var devices = cards[c].parentElement.parentElement.parentElement.getElementsByClassName("checkbox-individual");

        
        if (devices.length > 0) {
            var selected = false;

            for (var d = 0; d < devices.length; d++) {
                if (devices[d].checked == true) { selected = true; devices[d].parentNode.parentNode.style.background = "#ff3a3a"; } else { devices[d].parentNode.parentNode.style.background = "none"; }
            }
        }

        var label = cards[c].parentElement;
        if (selected == true) { if (!label.classList.contains("active")) { label.classList.add("active"); } } else { if (label.classList.contains("active")) { label.classList.remove("active"); } }

        }
}

document.addEventListener("onload", onloadItemsStateChanged());