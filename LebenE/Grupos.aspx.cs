﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;

namespace LebenE
{
    public partial class Grupos : System.Web.UI.Page
    {
        BasicHttpsBinding myBinding;
        EndpointAddress ea;
        WS.ApplicationServiceClient cc;
        StringBuilder t = new StringBuilder();
        WS.Group[] Listg= new WS.Group[0];
        List<HtmlInputCheckBox> lista_devices = new List<HtmlInputCheckBox>();
        WS.Device[] devices = null;
        List<HtmlInputCheckBox> cb_grupos = new List<HtmlInputCheckBox>();
        int excel = 2;

        protected void Page_Load(object sender, EventArgs e)
        {
            myBinding = new BasicHttpsBinding(BasicHttpsSecurityMode.TransportWithMessageCredential);
            myBinding.MaxReceivedMessageSize = Int32.MaxValue;
            ea = new
           //EndpointAddress("https://api.lebenws.com/2014/12/SOAP/Basic/");
           //EndpointAddress("https://api.brightsignnetwork.com/2014/12/SOAP/Basic/");
           EndpointAddress("https://api.lebencompany.com/2014/12/SOAP/Basic/");
            cc = new WS.ApplicationServiceClient(myBinding, ea);
            WS.User user = (WS.User)Session["User"];
            cc.ClientCredentials.UserName.UserName = (String)Session["Usuario"];
            cc.ClientCredentials.UserName.Password = (String)Session["Pass"];

            if (!Page.IsPostBack)
            {
                loadDropDownList();
                load_dt_inicio_settings();
            }
            if (user == null)
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                cc.Open();
                loadUser(user);
                loadCards();
                lb_role.Text = Session["Valor"] as String;
            }
            if( Session["List"]!= null && Session["Valor"]!= null)
            {
               
                List<HtmlInputCheckBox> l = Session["List"] as List<HtmlInputCheckBox>;
                
                String valor = Session["Valor"] as String;
                lb_role.Text = l.Count.ToString();
                //l.RemoveRange(0, 10);
                lb_userEmail.Text = l.Count.ToString();

               // MakeReport(l, valor);
            }   
        }

        protected void loadUser(WS.User user)
        {
            lb_userID.Text = user.Id.ToString();
            lb_username.Text = user.FirstName;
            lb_userlastname.Text = user.LastName;
            lb_userEmail.Text = user.Email;
            lb_role.Text = user.RoleName;
            lb_descrip.Text = user.Description;
           
        }
        protected void Logout(object sender, EventArgs e)
        {
            Session["User"] = null;
            Session["Usuario"] = null;
            Session["Pass"] = null;
            cc.Close();
            Response.Redirect("Default.aspx");

        }



        protected void Home(object sender, EventArgs e)
        {
            Response.Redirect("Grupos.aspx");
        }

        protected void Reports(object sender, EventArgs e)
        {
            Response.Redirect("Reports.aspx");
        }

        protected WS.Device[] Disp()
        {
            WS.PagedDeviceList page = cc.GetAllDevices(null, -1);
            WS.Device[] devices = new WS.Device[page.TotalItemCount];
          
            int count = 0;
            page.Items.CopyTo(devices, count);
            count = page.Items.Length;
            String marcador = page.NextMarker;
            Boolean flag = page.IsTruncated;
            while (flag)
            {
                WS.PagedDeviceList test = cc.GetAllDevices(marcador, -1);
                test.Items.CopyTo(devices, count);
                marcador = test.NextMarker;
                flag = test.IsTruncated;
                count = count + test.Items.Length;

            }

            return devices;
        }



        protected void getDevices()
        {
            try
            {
                // Begin using the client.

                WS.PagedDeviceList page = cc.GetAllDevices(null, -1);
                devices = new WS.Device[page.TotalItemCount];
                //prueba.Text = "numero" + page.TotalItemCount.ToString();
                int count = 0;
                page.Items.CopyTo(devices, count);
                count = page.Items.Length;
                String marcador = page.NextMarker;
                Boolean flag = page.IsTruncated;
                while (flag)
                {
                    WS.PagedDeviceList test = cc.GetAllDevices(marcador, -1);
                    test.Items.CopyTo(devices, count);
                    marcador = test.NextMarker;
                    flag = test.IsTruncated;
                    count = count + test.Items.Length;

                }

                //t8.Text = "Total" + devices.Length;

                //devices = page.Items;


                //t2.Text = "Total2"+page.TotalItemCount.ToString();



            }
            catch (Exception e)
            {
                //  prueba.Text = "fallo";
            }
        }



        protected void loadCards()
        {
            getDevices();
            getGroups();
            //cb_grupos.Clear();
            //lista_devices.Clear();

            t.Append("<div class='row dashboard-cards'>");
            Literal s = new Literal { Text = t.ToString() };
            tabla.Controls.Add(s);
            t.Clear();

            if (Listg.Length != 0)
            {

                for (int i = 0; i < Listg.Length; i++)
                {
                    //Checkboxes para controlar la seleccion de grupos existentes en la red, a traves de su atributo 'checked'.
                    HtmlInputCheckBox group_checkbox = new HtmlInputCheckBox() { ID = "99999" + Listg[i].Id.ToString(), Checked = false };
                    group_checkbox.Attributes.Add("hidden", "true");
                    group_checkbox.Attributes.Add("class", "checkbox-grupal");
                    cb_grupos.Add(group_checkbox);

                    t.Append("<div class='card col-md-3' id=" + Listg[i].Id.ToString() + "> \n" +
                        "<div class='card-title'> \n" +
                        "<h2 onclick='select_group(" + group_checkbox.ID + ",this);'>\n" +
                        Listg[i].Name +
                        "<small>Contiene '" + Listg[i].DevicesCount + "' player(s).</small> \n");

                    s = new Literal { Text = t.ToString() };
                    tabla.Controls.Add(s);
                    t.Clear();

                    tabla.Controls.Add(group_checkbox);


                    t.Append("</h2>\n" +
                        "<div class='task-count' onclick='showGroup(" + Listg[i].Id.ToString() + ",this);'> \n" +
                        "+ </div> \n" +
                        " </div> \n" +
                        "<div class='card-flap flap1'> \n" +
                        "<div class='card-description'> \n" +
                        "<ul class='task-list'>\n");

                    s = new Literal { Text = t.ToString() };
                    tabla.Controls.Add(s);
                    t.Clear();


                    int cont = 0;


                    for (int j = 0; j < devices.Length; j++)
                    {

                        if (devices[j].TargetGroupId == Listg[i].Id)
                        {

                            t.Append("<li id=" + devices[j].Serial + " onclick='deviceStateChanged(this);'>" + (!devices[j].Name.Equals("") ? devices[j].Name : "(Sin nombre)") + "<small> (" + devices[j].Serial + ")</small>" +
                            "<span>\n");
                            s = new Literal { Text = t.ToString() };
                            tabla.Controls.Add(s);
                            t.Clear();

                            HtmlInputCheckBox device_check = new HtmlInputCheckBox() { ID = devices[j].Serial };

                            device_check.Attributes.Add("class", "g-" + group_checkbox.ID + " checkbox-individual");
                            lista_devices.Add(device_check);
                            tabla.Controls.Add(device_check);

                            t.Append("</span> " +
                            "</li>\n");
                            s = new Literal { Text = t.ToString() };
                            tabla.Controls.Add(s);
                            t.Clear();
                        }
                    }



                    t.Append("</ul> \n" +
                        "</div>  \n" +
                        "<div class='card-flap flap2'> \n" +
                        "<div class='card-actions'>\n" +
                    "<label class='button-inverse' onclick='closeGroup(" + Listg[i].Id + ");'> Cerrar </label> \n" +
                    "</div> \n" +
                    "</div> \n" +
                    "</div> \n" +
                    "</div> \n");

                    s = new Literal { Text = t.ToString() };
                    tabla.Controls.Add(s);
                    t.Clear();
                }

            }
            t.Append("</div>\n");
            s = new Literal { Text = t.ToString() };
            tabla.Controls.Add(s);
            t.Clear();




        }

        protected void getGroups()
        {
            try
            {
                // Begin using the client.
                WS.PagedGroupList page = cc.GetGroups(null, -1);
                //prueba.Text = "numero" + page.TotalItemCount.ToString();
                Listg = new WS.Group[page.TotalItemCount];
                Listg = page.Items;
            }
            catch (Exception e)
            {
                //  prueba.Text = "fallo";
            }
        }

        protected void getReports(object sender, EventArgs e)
        {

            String Tipo = report_period.SelectedItem.Value;
            //CODIGO PARA PARSEAR VALOR DE INPUT[DATE]
            String dateString = dt_inicio.Value;
            DateTime date = DateTime.Parse(dateString, System.Globalization.CultureInfo.InvariantCulture);

            if (Tipo.Equals("Playback Weekly Report")){ while(date.DayOfWeek != DayOfWeek.Sunday){ date = date.AddDays(1); } }
            String mes = date.Month.ToString();
            if (date.Month.ToString().Length == 1)
            {
                mes = "0" + date.Month.ToString();
            }
            String dia = date.Day.ToString();
            if (date.Day.ToString().Length == 1)
            {
                dia = "0" + date.Day.ToString();
            }
            String valor = "";
            if (report_period.SelectedItem.Value == "Playback Daily Report" || report_period.SelectedItem.Value == "Playback Weekly Report")
            {
                 valor = report_period.SelectedItem.Value + " " + date.Year.ToString() + mes + dia;
            }
            else
            {
                 DateTimeFormatInfo mfi = new DateTimeFormatInfo();
                string monthName = mfi.GetMonthName(date.Month).ToString();
                valor = report_period.SelectedItem.Value + " " + monthName + " " + date.Year;
            }
            String formato_salida = "";

            if (rd_DB.Checked == true) { formato_salida = "database"; }
            else
                if (rd_csv.Checked == true) { formato_salida = "csv"; }
            else
                if (rd_pdf.Checked == true) { formato_salida = "pdf"; }
            List<HtmlInputCheckBox> l= new List<HtmlInputCheckBox>();
            foreach (HtmlInputCheckBox b in lista_devices)
            {
                if (b.Checked == true)
                {
                    l.Add(b);
                }
            }
            if(formato_salida== "database")
            {
                MakeReportBD(l, valor);
            }
            if (formato_salida == "csv")
            {
                 MakeReportExcel(l, valor);
            }
            if (formato_salida == "pdf")
            {
                MakeReportPDF(l, valor);
            }




        }

        protected void MakeReportBD(List<HtmlInputCheckBox> l, String valor)
        {
           // lb_userlastname.Text = l.Count.ToString();
            //int contador = 0;

           // string javaScript = "show_container_loading();";
           //ScriptManager.RegisterStartupScript(this, this.GetType(), "show_container_loading", javaScript, true);
            //lb_userID.Text = l.Count().ToString();
            // l.RemoveRange(0, 10);
           // bool flag = false;
            foreach (HtmlInputCheckBox b in l)
                {
            
                    WS.Device d = cc.GetDeviceBySerial(b.ID);
                    String deviceName = d.Name;
                    getDeviceReportBD(d.Id, WS.DeviceLogType.Playback, valor, deviceName);                  
                   // contador++;
                   // Session["List"] = l;
                   // Session["Valor"] = valor;                
               
                
                
                    
             
                }
            Response.Redirect("Grupos.aspx");
            // javaScript = "hide_container_loading();";
            // ScriptManager.RegisterStartupScript(this, this.GetType(), "show_container_loading", javaScript, true);
            // Session["List"]  = null;
            // Session["Valor"] = null;

        }

        protected void MakeReportExcel(List<HtmlInputCheckBox> l, String valor)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

           
            ExcelPackage pck = new ExcelPackage();
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Log Report");
            ws.Cells["A1"].Value = "Numero de Tienda";
            ws.Cells["B1"].Value = "Fecha de Inicio";
            ws.Cells["C1"].Value = "Fecha Final";
            ws.Cells["D1"].Value = "Media";
            ws.Cells["E1"].Value = "Nombre";
            ws.Cells["F1"].Value = "Tipo de contenido";
            foreach (HtmlInputCheckBox b in l)
            {

                WS.Device d = cc.GetDeviceBySerial(b.ID);
                String deviceName = d.Name;
                getDeviceReportExcel(d.Id, WS.DeviceLogType.Playback, valor, deviceName, ws);
            }

            //String javaScript = "hide_container_loading();";
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "show_container_loading", javaScript, true);

            ws.Cells["A:AZ"].AutoFitColumns();
            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.ContentType ="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename=" + "ExcelReportLogs.xlsx");
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.Flush();
            //Response.End();
           
        }

        protected void MakeReportPDF(List<HtmlInputCheckBox> l, String valor)
        {
            string fileName = "Reporte de LOGS" + ".pdf";
            string filePath = Path.Combine(Server.MapPath("~/"), fileName);

            Document doc = new Document(PageSize.A4, 2, 2, 2, 2);

            // Create paragraph for show in PDF file header
            Paragraph p = new Paragraph("Reporte de reproducciones");
            //p.SetAlignment("center");
            PdfWriter.GetInstance(doc, new FileStream(filePath, FileMode.Create));
            //Create table here for write database data
            PdfPTable pdfTab = new PdfPTable(6); // here 4 is no of column
            pdfTab.HorizontalAlignment = 1; // 0- Left, 1- Center, 2- right
            pdfTab.SpacingBefore = 20f;
            pdfTab.SpacingAfter = 20f;

            pdfTab.AddCell("Numero de Tienda");
            pdfTab.AddCell("Fecha de inicio");
            pdfTab.AddCell("Fecha Final");
            pdfTab.AddCell("Media");
            pdfTab.AddCell("Nombre del Video");
            pdfTab.AddCell("Tipo");

            foreach (HtmlInputCheckBox b in l)
            {

                WS.Device d = cc.GetDeviceBySerial(b.ID);
                String deviceName = d.Name;
                getDeviceReportPDF(d.Id, WS.DeviceLogType.Playback, valor, deviceName, pdfTab);
            }

            doc.Open();
            doc.Add(p);
            doc.Add(pdfTab);
            doc.Close();

            byte[] content = File.ReadAllBytes(filePath);
            HttpContext context = HttpContext.Current;

            context.Response.BinaryWrite(content);
            context.Response.ContentType = "application/pdf";
            context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);
            context.Response.End();

            Response.Redirect("Grupos.aspx");

            

        }



        protected void getDeviceReportBD(int idDevice, WS.DeviceLogType tipo, String Name, String deviceName)
        {
            String connetionString = "Data Source=lebencompany.southcentralus.cloudapp.azure.com,1480; Initial Catalog=TestAPI;User ID=LEBENLABHD; Password =&SQL2020%L4BL3b3nH3p!#";
            SqlConnection connection = new SqlConnection(connetionString);
            SqlDataAdapter adpter = new SqlDataAdapter();
            SqlCommand command;
            DataSet ds = new DataSet();

            String DName = "";
            for(int i=0; i<deviceName.Length; i++)
            {
                if (Char.IsDigit(deviceName[i]))
                {
                    DName = DName + deviceName[i];
                }
                

            }

            String type = "";
            bool bandera = false;
            WS.PagedDeviceLogReportList report = cc.GetDeviceLogReports(idDevice, tipo, null, -1);
            if (report.TotalItemCount!=0||report.TotalItemCount!=null)
            {

                WS.DeviceLogReport[] DeviceReport = new WS.DeviceLogReport[report.TotalItemCount];
                int count = 0;
                report.Items.CopyTo(DeviceReport, count);
                count = report.Items.Length;
                String marcador = report.NextMarker;
                Boolean flag = report.IsTruncated;
                while (flag)
                {
                    WS.PagedDeviceLogReportList test = cc.GetDeviceLogReports(idDevice, tipo, marcador, -1);
                    //WS.PagedDeviceList  = cc.GetAllDevices(marcador, -1);
                    test.Items.CopyTo(DeviceReport, count);
                    marcador = test.NextMarker;
                    flag = test.IsTruncated;
                    count = count + test.Items.Length;

                }
            
           
            //DeviceReport = report.Items;
            //Label9.Text = "Paso3";
            try
            {
               
                        for (int i = 0; i < DeviceReport.Length; i++)
                        {
                            // Label2.Text = Name;
                            if (DeviceReport[i].Name == Name)
                            {
                                connection.Open();
                                XmlTextReader reader = new XmlTextReader(DeviceReport[i].FilePath);
                                ds.ReadXml(reader);

                                for (int x = 0; x <= ds.Tables[12].Rows.Count - 1; x = x + 5)
                                {
                                    if (ds.Tables[12].Rows[x + 4].ItemArray[1].ToString() == null || ds.Tables[12].Rows[x + 4].ItemArray[1].ToString() == "")
                                    {

                                    }
                                    else
                                    {
                                        String name = ds.Tables[12].Rows[x + 4].ItemArray[1].ToString();
                                        String pre = "" + name[0] + name[1] + name[2];
                                        String pre2 = "" + name[0] + name[1];
                                        String pre3 = "" + name[0] + name[1] + name[2];
                                        String sql = "";
                                        String NameF = "";
                                        if (pre == "PK_")
                                        {
                                            for (int p = 3; p < name.Length - 4; p++)
                                            {
                                                NameF = NameF + name[p];
                                            }

                                            type = "PK";
                                            sql = "insert into dbo.Test4 values('" + DName.Trim() + "', '" + ds.Tables[12].Rows[x].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 1].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 3].ItemArray[1] + "','" + NameF.Trim() + "','" + type + "')";
                                            command = new SqlCommand(sql, connection);
                                            adpter.InsertCommand = command;
                                            adpter.InsertCommand.ExecuteNonQuery();

                                        }
                                        if (pre2 == "T_")
                                        {
                                            for (int p = 2; p < name.Length - 4; p++)
                                            {
                                                NameF = NameF + name[p];
                                            }
                                            type = "Talleres";
                                            sql = "insert into dbo.Test4 values('" + DName.Trim() + "', '" + ds.Tables[12].Rows[x].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 1].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 3].ItemArray[1] + "','" + NameF.Trim() + "','" + type + "')";
                                            command = new SqlCommand(sql, connection);
                                            adpter.InsertCommand = command;
                                            adpter.InsertCommand.ExecuteNonQuery();

                                        }
                                        if (pre3 == "MET")
                                        {
                                            for (int p = 4; p < name.Length - 4; p++)
                                            {
                                                NameF = NameF + name[p];
                                            }
                                            type = "MET";
                                            sql = "insert into dbo.Test4 values('" + DName.Trim() + "', '" + ds.Tables[12].Rows[x].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 1].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 3].ItemArray[1] + "','" + NameF.Trim() + "','" + type + "')";
                                            command = new SqlCommand(sql, connection);
                                            adpter.InsertCommand = command;
                                            adpter.InsertCommand.ExecuteNonQuery();

                                        }
                                        //codigo de auxilio
                                        if (exceptions(name))
                                        {
                                            for (int p = 0; p < name.Length - 4; p++)
                                            {
                                                NameF = NameF + name[p];
                                            }
                                            type = "PK";
                                            sql = "insert into dbo.Test4 values('" + DName.Trim() + "', '" + ds.Tables[12].Rows[x].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 1].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 3].ItemArray[1] + "','" + NameF.Trim() + "','" + type + "')";
                                            command = new SqlCommand(sql, connection);
                                            adpter.InsertCommand = command;
                                            adpter.InsertCommand.ExecuteNonQuery();
                                        }
                                        if (name == "Cómo crear un baño seguro para todos.mp4")
                                        {
                                            for (int p = 0; p < name.Length - 4; p++)
                                            {
                                                NameF = NameF + name[p];
                                            }
                                            type = "Talleres";
                                            sql = "insert into dbo.Test4 values('" + DName.Trim() + "', '" + ds.Tables[12].Rows[x].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 1].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 3].ItemArray[1] + "','" + NameF.Trim() + "','" + type + "')";
                                            command = new SqlCommand(sql, connection);
                                            adpter.InsertCommand = command;
                                            adpter.InsertCommand.ExecuteNonQuery();
                                        }

                                        //aqui acaba el crack 
                                    }

                                }

                                connection.Close();
                            }
                        }
                    
                }
                catch (SqlException ex) { }
                catch (Exception e)
                {

                    Response.Redirect("Error.aspx");
                }
            }
           
        }

        protected void getDeviceReportPDF(int idDevice, WS.DeviceLogType tipo, String Name, String deviceName, PdfPTable pdfTab)
        {
            DataSet ds = new DataSet();

            String DName = "";
            for (int i = 0; i < deviceName.Length; i++)
            {
                if (Char.IsDigit(deviceName[i]))
                {
                    DName = DName + deviceName[i];
                }


            }

            String type = "";
            bool bandera = false;
            WS.PagedDeviceLogReportList report = cc.GetDeviceLogReports(idDevice, tipo, null, -1);
            if (report.TotalItemCount != 0 || report.TotalItemCount != null)
            {

                WS.DeviceLogReport[] DeviceReport = new WS.DeviceLogReport[report.TotalItemCount];
                int count = 0;
                report.Items.CopyTo(DeviceReport, count);
                count = report.Items.Length;
                String marcador = report.NextMarker;
                Boolean flag = report.IsTruncated;
                while (flag)
                {
                    WS.PagedDeviceLogReportList test = cc.GetDeviceLogReports(idDevice, tipo, marcador, -1);
                    //WS.PagedDeviceList  = cc.GetAllDevices(marcador, -1);
                    test.Items.CopyTo(DeviceReport, count);
                    marcador = test.NextMarker;
                    flag = test.IsTruncated;
                    count = count + test.Items.Length;

                }


                //DeviceReport = report.Items;
                //Label9.Text = "Paso3";
                try
                {

                    for (int i = 0; i < DeviceReport.Length; i++)
                    {
                        // Label2.Text = Name;
                        if (DeviceReport[i].Name == Name)
                        {
                            XmlTextReader reader = new XmlTextReader(DeviceReport[i].FilePath);
                            ds.ReadXml(reader);

                            for (int x = 0; x <= ds.Tables[12].Rows.Count - 1; x = x + 5)
                            {
                                if (ds.Tables[12].Rows[x + 4].ItemArray[1].ToString() == null || ds.Tables[12].Rows[x + 4].ItemArray[1].ToString() == "")
                                {

                                }
                                else
                                {
                                    String name = ds.Tables[12].Rows[x + 4].ItemArray[1].ToString();
                                    String pre = "" + name[0] + name[1] + name[2];
                                    String pre2 = "" + name[0] + name[1];
                                    String pre3 = "" + name[0] + name[1] + name[2];
                                    String NameF = "";
                                    if (pre == "PK_")
                                    {
                                        for (int p = 3; p < name.Length - 4; p++)
                                        {
                                            NameF = NameF + name[p];
                                        }

                                        type = "PK";
                                        // sql = "insert into dbo.Test4 values('" + DName.Trim() + "', '" + ds.Tables[12].Rows[x].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 1].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 3].ItemArray[1] + "','" + NameF.Trim() + "','" + type + "')";
                                        pdfTab.AddCell(DName.Trim());
                                        pdfTab.AddCell(ds.Tables[12].Rows[x].ItemArray[1].ToString());
                                        pdfTab.AddCell(ds.Tables[12].Rows[x + 1].ItemArray[1].ToString());
                                        pdfTab.AddCell(ds.Tables[12].Rows[x + 3].ItemArray[1].ToString());
                                        pdfTab.AddCell(NameF.Trim());
                                        pdfTab.AddCell(type);
                                    }
                                    if (pre2 == "T_")
                                    {
                                        for (int p = 2; p < name.Length - 4; p++)
                                        {
                                            NameF = NameF + name[p];
                                        }
                                        type = "Talleres";
                                        //sql = "insert into dbo.Test4 values('" + DName.Trim() + "', '" + ds.Tables[12].Rows[x].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 1].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 3].ItemArray[1] + "','" + NameF.Trim() + "','" + type + "')";
                                        pdfTab.AddCell(DName.Trim());
                                        pdfTab.AddCell(ds.Tables[12].Rows[x].ItemArray[1].ToString());
                                        pdfTab.AddCell(ds.Tables[12].Rows[x + 1].ItemArray[1].ToString());
                                        pdfTab.AddCell(ds.Tables[12].Rows[x + 3].ItemArray[1].ToString());
                                        pdfTab.AddCell(NameF.Trim());
                                        pdfTab.AddCell(type);
                                    }
                                    if (pre3 == "MET")
                                    {
                                        for (int p = 4; p < name.Length - 4; p++)
                                        {
                                            NameF = NameF + name[p];
                                        }
                                        type = "MET";
                                        // sql = "insert into dbo.Test4 values('" + DName.Trim() + "', '" + ds.Tables[12].Rows[x].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 1].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 3].ItemArray[1] + "','" + NameF.Trim() + "','" + type + "')";
                                        pdfTab.AddCell(DName.Trim());
                                        pdfTab.AddCell(ds.Tables[12].Rows[x].ItemArray[1].ToString());
                                        pdfTab.AddCell(ds.Tables[12].Rows[x + 1].ItemArray[1].ToString());
                                        pdfTab.AddCell(ds.Tables[12].Rows[x + 3].ItemArray[1].ToString());
                                        pdfTab.AddCell(NameF.Trim());
                                        pdfTab.AddCell(type);

                                    }
                                    //codigo de auxilio
                                    if (exceptions(name))
                                    {
                                        for (int p = 0; p < name.Length - 4; p++)
                                        {
                                            NameF = NameF + name[p];
                                        }
                                        type = "PK";
                                        // sql = "insert into dbo.Test4 values('" + DName.Trim() + "', '" + ds.Tables[12].Rows[x].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 1].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 3].ItemArray[1] + "','" + NameF.Trim() + "','" + type + "')";
                                        pdfTab.AddCell(DName.Trim());
                                        pdfTab.AddCell(ds.Tables[12].Rows[x].ItemArray[1].ToString());
                                        pdfTab.AddCell(ds.Tables[12].Rows[x + 1].ItemArray[1].ToString());
                                        pdfTab.AddCell(ds.Tables[12].Rows[x + 3].ItemArray[1].ToString());
                                        pdfTab.AddCell(NameF.Trim());
                                        pdfTab.AddCell(type);
                                    }
                                    if (name == "Cómo crear un baño seguro para todos.mp4")
                                    {
                                        for (int p = 0; p < name.Length - 4; p++)
                                        {
                                            NameF = NameF + name[p];
                                        }
                                        type = "Talleres";
                                        // sql = "insert into dbo.Test4 values('" + DName.Trim() + "', '" + ds.Tables[12].Rows[x].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 1].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 3].ItemArray[1] + "','" + NameF.Trim() + "','" + type + "')";                                     
                                        pdfTab.AddCell(DName.Trim());
                                        pdfTab.AddCell(ds.Tables[12].Rows[x].ItemArray[1].ToString());
                                        pdfTab.AddCell(ds.Tables[12].Rows[x + 1].ItemArray[1].ToString());
                                        pdfTab.AddCell(ds.Tables[12].Rows[x + 3].ItemArray[1].ToString());
                                        pdfTab.AddCell(NameF.Trim());
                                        pdfTab.AddCell(type);
                                    }
                                    //aqui acaba el crack 
                                }

                            }

                        }
                    }

                }
                catch (SqlException ex) { }
                catch (Exception e)
                {

                    Response.Redirect("Error.aspx");
                }
            }

        }


        protected void getDeviceReportExcel(int idDevice, WS.DeviceLogType tipo, String Name, String deviceName, ExcelWorksheet ws)
        {
            String connetionString = "Data Source=lebencompany.southcentralus.cloudapp.azure.com,1480; Initial Catalog=TestAPI;User ID=LEBENLABHD; Password =&SQL2020%L4BL3b3nH3p!#";
            DataSet ds = new DataSet(); 
            String DName = "";
          

            for (int i = 0; i < deviceName.Length; i++)
            {
                if (Char.IsDigit(deviceName[i]))
                {
                 DName = DName + deviceName[i];
                }
            }

            String type = "";
            bool bandera = false;
            WS.PagedDeviceLogReportList report = cc.GetDeviceLogReports(idDevice, tipo, null, -1);
            if (report.TotalItemCount != 0)
            {

                WS.DeviceLogReport[] DeviceReport = new WS.DeviceLogReport[report.TotalItemCount];
                int count = 0;
                report.Items.CopyTo(DeviceReport, count);
                count = report.Items.Length;
                String marcador = report.NextMarker;
                Boolean flag = report.IsTruncated;
                while (flag)
                {
                    WS.PagedDeviceLogReportList test = cc.GetDeviceLogReports(idDevice, tipo, marcador, -1);
                    //WS.PagedDeviceList  = cc.GetAllDevices(marcador, -1);
                    test.Items.CopyTo(DeviceReport, count);
                    marcador = test.NextMarker;
                    flag = test.IsTruncated;
                    count = count + test.Items.Length;
                }
                try
                {

                    for (int i = 0; i < DeviceReport.Length; i++)
                    {
                        // Label2.Text = Name;
                        if (DeviceReport[i].Name == Name)
                        {
                           // connection.Open();
                            XmlTextReader reader = new XmlTextReader(DeviceReport[i].FilePath);
                            ds.ReadXml(reader);

                            for (int x = 0; x <= ds.Tables[12].Rows.Count - 1; x = x + 5)
                            {
                                if (ds.Tables[12].Rows[x + 4].ItemArray[1].ToString() == null || ds.Tables[12].Rows[x + 4].ItemArray[1].ToString() == "")
                                {

                                }
                                else
                                {
                                    String name = ds.Tables[12].Rows[x + 4].ItemArray[1].ToString();
                                    String pre = "" + name[0] + name[1] + name[2];
                                    String pre2 = "" + name[0] + name[1];
                                    String pre3 = "" + name[0] + name[1] + name[2];
                                    String sql = "";
                                    String NameF = "";
                                    if (pre == "PK_")
                                    {
                                        for (int p = 3; p < name.Length - 4; p++)
                                        {
                                            NameF = NameF + name[p];
                                        }

                                        type = "PK";
                                        // sql = "insert into dbo.Test4 values('" + DName.Trim() + "', '" + ds.Tables[12].Rows[x].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 1].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 3].ItemArray[1] + "','" + NameF.Trim() + "','" + type + "')";

                                        // command = new SqlCommand(sql, connection);
                                        // adpter.InsertCommand = command;
                                        // adpter.InsertCommand.ExecuteNonQuery();
                                        ws.Cells[String.Format("A{0}", excel)].Value = DName.Trim();
                                        ws.Cells[String.Format("B{0}", excel)].Value = ds.Tables[12].Rows[x].ItemArray[1];
                                        ws.Cells[String.Format("C{0}", excel)].Value = ds.Tables[12].Rows[x + 1].ItemArray[1];
                                        ws.Cells[String.Format("D{0}", excel)].Value = ds.Tables[12].Rows[x + 3].ItemArray[1];
                                        ws.Cells[String.Format("E{0}", excel)].Value = NameF.Trim();
                                        ws.Cells[String.Format("F{0}", excel)].Value = type;

                                        excel++;
                                   
                                    }
                                    if (pre2 == "T_")
                                    {
                                        for (int p = 2; p < name.Length - 4; p++)
                                        {
                                            NameF = NameF + name[p];
                                        }
                                        type = "Talleres";
                                        /* sql = "insert into dbo.Test4 values('" + DName.Trim() + "', '" + ds.Tables[12].Rows[x].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 1].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 3].ItemArray[1] + "','" + NameF.Trim() + "','" + type + "')";
                                         command = new SqlCommand(sql, connection);
                                         adpter.InsertCommand = command;
                                         adpter.InsertCommand.ExecuteNonQuery(); */
                                        ws.Cells[String.Format("A{0}", excel)].Value = DName.Trim();
                                        ws.Cells[String.Format("B{0}", excel)].Value = ds.Tables[12].Rows[x].ItemArray[1];
                                        ws.Cells[String.Format("C{0}", excel)].Value = ds.Tables[12].Rows[x + 1].ItemArray[1];
                                        ws.Cells[String.Format("D{0}", excel)].Value = ds.Tables[12].Rows[x + 3].ItemArray[1];
                                        ws.Cells[String.Format("E{0}", excel)].Value = NameF.Trim();
                                        ws.Cells[String.Format("F{0}", excel)].Value = type;

                                        excel++;

                                    }
                                    if (pre3 == "MET")
                                    {
                                        for (int p = 4; p < name.Length - 4; p++)
                                        {
                                            NameF = NameF + name[p];
                                        }
                                        type = "MET";
                                        /* sql = "insert into dbo.Test4 values('" + DName.Trim() + "', '" + ds.Tables[12].Rows[x].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 1].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 3].ItemArray[1] + "','" + NameF.Trim() + "','" + type + "')";
                                         command = new SqlCommand(sql, connection);
                                         adpter.InsertCommand = command;
                                         adpter.InsertCommand.ExecuteNonQuery();
                                        */
                                        ws.Cells[String.Format("A{0}", excel)].Value = DName.Trim();
                                        ws.Cells[String.Format("B{0}", excel)].Value = ds.Tables[12].Rows[x].ItemArray[1];
                                        ws.Cells[String.Format("C{0}", excel)].Value = ds.Tables[12].Rows[x + 1].ItemArray[1];
                                        ws.Cells[String.Format("D{0}", excel)].Value = ds.Tables[12].Rows[x + 3].ItemArray[1];
                                        ws.Cells[String.Format("E{0}", excel)].Value = NameF.Trim();
                                        ws.Cells[String.Format("F{0}", excel)].Value = type;

                                        excel++;

                                    }
                                    //codigo de auxilio
                                    if (exceptions(name))
                                    {
                                        for (int p = 0; p < name.Length - 4; p++)
                                        {
                                            NameF = NameF + name[p];
                                        }
                                        type = "PK";
                                        /* sql = "insert into dbo.Test4 values('" + DName.Trim() + "', '" + ds.Tables[12].Rows[x].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 1].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 3].ItemArray[1] + "','" + NameF.Trim() + "','" + type + "')";
                                        command = new SqlCommand(sql, connection);
                                        adpter.InsertCommand = command;
                                        adpter.InsertCommand.ExecuteNonQuery(); */
                                        ws.Cells[String.Format("A{0}", excel)].Value = DName.Trim();
                                        ws.Cells[String.Format("B{0}", excel)].Value = ds.Tables[12].Rows[x].ItemArray[1];
                                        ws.Cells[String.Format("C{0}", excel)].Value = ds.Tables[12].Rows[x + 1].ItemArray[1];
                                        ws.Cells[String.Format("D{0}", excel)].Value = ds.Tables[12].Rows[x + 3].ItemArray[1];
                                        ws.Cells[String.Format("E{0}", excel)].Value = NameF.Trim();
                                        ws.Cells[String.Format("F{0}", excel)].Value = type;

                                        excel++;

                                    }
                                    if (name == "Cómo crear un baño seguro para todos.mp4")
                                    {
                                        for (int p = 0; p < name.Length - 4; p++)
                                        {
                                            NameF = NameF + name[p];
                                        }
                                        type = "Talleres";
                                        /* sql = "insert into dbo.Test4 values('" + DName.Trim() + "', '" + ds.Tables[12].Rows[x].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 1].ItemArray[1] + "','" + ds.Tables[12].Rows[x + 3].ItemArray[1] + "','" + NameF.Trim() + "','" + type + "')";
                                         command = new SqlCommand(sql, connection);
                                         adpter.InsertCommand = command;
                                         adpter.InsertCommand.ExecuteNonQuery(); */
                                        ws.Cells[String.Format("A{0}", excel)].Value = DName.Trim();
                                        ws.Cells[String.Format("B{0}", excel)].Value = ds.Tables[12].Rows[x].ItemArray[1];
                                        ws.Cells[String.Format("C{0}", excel)].Value = ds.Tables[12].Rows[x + 1].ItemArray[1];
                                        ws.Cells[String.Format("D{0}", excel)].Value = ds.Tables[12].Rows[x + 3].ItemArray[1];
                                        ws.Cells[String.Format("E{0}", excel)].Value = NameF.Trim();
                                        ws.Cells[String.Format("F{0}", excel)].Value = type;

                                        excel++;


                                    }

                                    //aqui acaba el crack 
                                }

                            }

                            //aqui

                          

                            //connection.Close();
                        }
                    }
                   

                }
                catch (SqlException ex) { }
                catch (Exception e)
                {
                    //return e;
                    //Response.Redirect("Error.aspx");
                } 
            }

        }






        protected Boolean exceptions(String video)
        {
            Boolean flag = false;
            if (video == "Rodillo para pintar Paint Stick.mp4" ||
               video == "Productos Básicos para Pintar.mp4" ||
               video == "Diseños Creativos y Sencillos para Pintar.mp4" ||
               video == "Baterias y Herramientas Ryobi One Plus.mp4" ||
               video == "Aspiradora Seco Mojado Next Ridgid.mp4" ||
               video == "Tuberías y Conexiones Nacobre.mp4" ||
               video == "Catalogo Extendido (Ago 2019)V2.mp4" ||
               video == "HD FIRST Soluciona Final.mp4" ||
               video == "Mensaje Especial COVID-19V2.mp4" ||
               video == "TD-23-9 1 1 1.mp4")
            {
                flag = true;
            }

            return flag;
        }



        protected void loadDropDownList()
        {
            report_period.Items.Clear();
            System.Web.UI.WebControls.ListItem diario = new System.Web.UI.WebControls.ListItem();
            diario.Selected = false;
            diario.Value = "Playback Daily Report";
            diario.Text = "Diario";

            report_period.Items.Add(diario);

            
            System.Web.UI.WebControls.ListItem semanal = new System.Web.UI.WebControls.ListItem();
            semanal.Selected = false;
            semanal.Value = "Playback Weekly Report";
            semanal.Text = "Semanal";

            report_period.Items.Add(semanal);

           System.Web.UI.WebControls.ListItem mensual = new System.Web.UI.WebControls.ListItem();
            mensual.Selected = false;
            mensual.Value = "Playback Monthly Report";
            mensual.Text = "Mensual";

            report_period.Items.Add(mensual);
            report_period.CssClass = "dropdownlist-dark pickers";
            report_period.SelectedIndex = 1;
        }

        protected void load_dt_inicio_settings()
        {
            dt_inicio.Attributes.Add("min", "2012-01-01");
            DateTime yesterday = DateTime.Now.AddDays(-1);
            String formato = yesterday.Year + "-" + (yesterday.Month.ToString().Length == 2 ? yesterday.Month.ToString() : "0" + yesterday.Month.ToString()) + "-" +
                (yesterday.Day.ToString().Length == 2 ? yesterday.Day.ToString() : "0" + yesterday.Day.ToString());
            dt_inicio.Attributes.Add("max", formato);
            dt_inicio.Attributes.Add("value", formato);
            dt_inicio.Attributes.Add("required", "");
        }

    }
}