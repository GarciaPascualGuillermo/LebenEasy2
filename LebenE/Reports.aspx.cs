﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;

namespace LebenE
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        BasicHttpsBinding myBinding;
        EndpointAddress ea;
        WS.ApplicationServiceClient cc;
        StringBuilder t = new StringBuilder();
        WS.Group[] Listg;
        List<HtmlInputCheckBox> lista_devices = new List<HtmlInputCheckBox>();
        WS.Device[] devices = null;
        List<HtmlInputCheckBox> cb_grupos = new List<HtmlInputCheckBox>();


     
        protected void Page_Load(object sender, EventArgs e)
        {


            myBinding = new BasicHttpsBinding(BasicHttpsSecurityMode.TransportWithMessageCredential);
            myBinding.MaxReceivedMessageSize = Int32.MaxValue;
            ea = new
            //EndpointAddress("https://api.lebenws.com/2014/12/SOAP/Basic/");
            //EndpointAddress("https://api.brightsignnetwork.com/2014/12/SOAP/Basic/");
            EndpointAddress("https://api.lebencompany.com/2014/12/SOAP/Basic/");
            cc = new WS.ApplicationServiceClient(myBinding, ea);
            WS.User user = (WS.User)Session["User"];
            cc.ClientCredentials.UserName.UserName = (String)Session["Usuario"];
            cc.ClientCredentials.UserName.Password = (String)Session["Pass"];



            if (user == null)
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                cc.Open();
                loadUser(user);
                //Response.Redirect("Login.aspx");
                //loadTable("0");
                //loadGroups();
                //getdeviceLog(2);
            }
        }

        protected void loadUser(WS.User user)
        {
            lb_userID.Text = user.Id.ToString();
            lb_username.Text = user.FirstName;
            lb_userlastname.Text = user.LastName;
            lb_userEmail.Text = user.Email;
            lb_role.Text = user.RoleName;
            lb_descrip.Text = user.Description;

        }
        protected void Logout(object sender, EventArgs e)
        {
            Session["User"] = null;
            Session["Usuario"] = null;
            Session["Pass"] = null;
            cc.Close();
            Response.Redirect("Default.aspx");

        }



        protected void Home(object sender, EventArgs e)
        {
            Response.Redirect("Grupos.aspx");
        }

        protected void Reports(object sender, EventArgs e)
        {
            Response.Redirect("Reports.aspx");
        }









    }
}