﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;
using System.Web.UI.WebControls;
using System.Xml;

namespace LebenE
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        StringBuilder t = new StringBuilder();
        StringBuilder g = new StringBuilder();
        BasicHttpsBinding myBinding;
        EndpointAddress ea;
        WS.ApplicationServiceClient cc;
        WS.Device[] devices;
        WS.Group[] Listg;
        List<LinkButton> botones = new List<LinkButton>();
        Literal s;
        static String[][] sDevices = null;

        protected void loadDropDownList()
        {
            report_period.Items.Clear();
            ListItem diario = new ListItem();
            diario.Selected = true;
            diario.Value = "daily";
            diario.Text = "Diario";

            report_period.Items.Add(diario);

            ListItem semanal = new ListItem();
            semanal.Selected = false;
            semanal.Value = "weekly";
            semanal.Text = "Semanal";

            report_period.Items.Add(semanal);

            ListItem mensual = new ListItem();
            mensual.Selected = false;
            mensual.Value = "Monthly";
            mensual.Text = "Mensual";

            report_period.Items.Add(mensual);
            report_period.CssClass = "dropdownlist-dark pickers";
        }

        protected void load_dt_inicio_settings()
        {
            dt_inicio.Attributes.Add("min", "2012-01-01");
            DateTime yesterday = DateTime.Now.AddDays(-1);
            String formato = yesterday.Year + "-" + (yesterday.Month.ToString().Length == 2 ? yesterday.Month.ToString() : "0" + yesterday.Month.ToString()) + "-" +
                (yesterday.Day.ToString().Length == 2 ? yesterday.Day.ToString() : "0" + yesterday.Day.ToString());
            dt_inicio.Attributes.Add("max", formato);
            dt_inicio.Attributes.Add("value", formato);
            dt_inicio.Attributes.Add("required", "");
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            myBinding = new BasicHttpsBinding(BasicHttpsSecurityMode.TransportWithMessageCredential);
            myBinding.MaxReceivedMessageSize = Int32.MaxValue;
            ea = new
            //EndpointAddress("https://api.lebenws.com/2014/12/SOAP/Basic/");
            EndpointAddress("https://api.brightsignnetwork.com/2014/12/SOAP/Basic/");
            //EndpointAddress("https://api.lebencompany.com/2014/12/SOAP/Basic/");
            cc = new WS.ApplicationServiceClient(myBinding, ea);
            WS.User user = (WS.User)Session["User"];
            cc.ClientCredentials.UserName.UserName = (String)Session["Usuario"];
            cc.ClientCredentials.UserName.Password = (String)Session["Pass"];


            loadDropDownList();
            load_dt_inicio_settings();

            if (user == null)
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                cc.Open();
                loadUser(user);
                loadTable("0");
                loadGroups();
        
            }

        }
        protected void loadUser(WS.User user)
        {
            lb_userID.Text = user.Id.ToString();
            lb_username.Text = user.FirstName;
            lb_userlastname.Text = user.LastName;
            lb_userEmail.Text = user.Email;
            lb_role.Text = user.RoleName;
            lb_descrip.Text = user.Description;
            name.Text = "Bienvenido " + user.FirstName;
        }
        protected void Logout(object sender, EventArgs e)
        {
            Session["User"] = null;
            Session["Usuario"] = null;
            Session["Pass"] = null;
            cc.Close();
            Response.Redirect("Default.aspx");

        }

        protected void loadTable(String group)
        {
            int groupid = Int32.Parse(group);
            // getDevices();
            WS.Group g = cc.GetGroup(groupid, true);
            WS.Device[] dev= new WS.Device[0];
            if (g!= null)
            {
              // dev = new WS.Device[g.DevicesCount];
                dev = cc.GetGroup(groupid, true).Devices;
            }
             
            
            t.Append("<table class='table-players display table-striped nowrap' cellspacing='0' runat='server'id='table_players'>");
            t.Append("<thead>");
            t.Append("<tr>");
            t.Append("<td class='t_header'>");
            t.Append("</td>");
            t.Append("<td class='t_header'>Id");
            t.Append("</td>");
            t.Append("<td class='t_header'>Nombre");
            t.Append("</td>");
            t.Append("<td class='t_header'>Numero de Serie");
            t.Append("</td>");
            t.Append("<td class='t_header'>Grupo");
            t.Append("</td>");
            t.Append("<td class='t_header'>Modelo");
            t.Append("</td>");
            t.Append("<td class='t_header'>Descripción");
            t.Append("</td>");
            t.Append("</tr>");
            t.Append("</thead>");
            t.Append("<tbody>");

            if (dev != null)
            {
                for (int i = 0; i < dev.Length; i++)
                {
                   
                        t.Append("<tr>");
                        t.Append("<td>");
                        t.Append("</td>");
                        t.Append("<td>" + dev[i].Id + "");
                        t.Append("</td>");
                        t.Append("<td> " + dev[i].Name + "");
                        t.Append("</td>");
                        t.Append("<td> " + dev[i].Serial + "");
                        t.Append("</td>");
                        t.Append("<td>" + dev[i].TargetGroupId.ToString() + "");
                        t.Append("</td>");
                        t.Append("<td> " + dev[i].Model + "");
                        t.Append("</td>");
                        t.Append("<td>" + dev[i].Description + "");
                        t.Append("</td>");
                        t.Append("</tr>");
                    
                }
            }

            t.Append("</tbody>");
            t.Append("<tfoot>");
            t.Append("<tr>");
            t.Append("<td>");
            t.Append("</td>");
            t.Append("<td class='t_header'>Id");
            t.Append("</td>");
            t.Append("<td class='t_header'>Nombre");
            t.Append("</td>");
            t.Append("<td class='t_header'>Numero de Serie");
            t.Append("</td>");
            t.Append("<td class='t_header'>Grupo");
            t.Append("</td>");
            t.Append("<td class='t_header'>Modelo");
            t.Append("</td>");
            t.Append("<td class='t_header'>Descripción");
            t.Append("</td>");
            t.Append("</tr>");
            t.Append("</tfoot>");
            t.Append("</table>");

            s = new Literal { Text = t.ToString() };
            tabla2.Controls.Add(s);

        }

        protected void PressButton(object sender, EventArgs e)
        {

        }
        protected void loadGroups()
        {
            getGroups();

            if (Listg != null)
            {
                LinkButton button1 = new LinkButton
                {
                    ID = "0",
                    Text = "Todos",
                    CssClass = "hyperlink active"
                };
                botones.Add(button1);
                button1.Click += new EventHandler(erase);
                //button1.Command += Load_Items;
               // gru.Controls.Add(button1);
                for (int i = 0; i < Listg.Length; i++)
                {
                    LinkButton button = new LinkButton
                    {
                        ID = Listg[i].Id.ToString(),
                        OnClientClick = "erase()",
                        Text = Listg[i].Name,
                        CssClass = "hyperlink",
                    };
                    button.Click += new EventHandler(erase);
                    gru.Controls.Add(button);
                    botones.Add(button);
                }
            }
        }


        public void erase(object sender, EventArgs e)
        {
            LinkButton bttn = sender as LinkButton;
            for (int i = 0; i < botones.Count; i++)
            {
                botones[i].CssClass = "hyperlink";
            }
            tabla2.Controls.Clear();
            t.Clear();
            bttn.CssClass = "hyperlink active";

            loadTable(bttn.ID);
        }



        protected void getDevices()
        {
            try
            {
                WS.PagedDeviceList page = cc.GetAllDevices(null, -1);
                devices = new WS.Device[page.TotalItemCount];
                //prueba.Text = "numero" + page.TotalItemCount.ToString();
                int count = 0;
                page.Items.CopyTo(devices, count);
                count = page.Items.Length;
                String marcador = page.NextMarker;
                Boolean flag = page.IsTruncated;
                while (flag)
                {
                    WS.PagedDeviceList test = cc.GetAllDevices(marcador, -1);
                    test.Items.CopyTo(devices, count);
                    marcador = test.NextMarker;
                    flag = test.IsTruncated;
                    count = count + test.Items.Length;

                }

                //t8.Text = "Total" + devices.Length;

                //devices = page.Items;


                //t2.Text = "Total2"+page.TotalItemCount.ToString();



            }
            catch (Exception e)
            {
                //  prueba.Text = "fallo";
            }
        }

        protected List<int> getGroupsId(int id)
        {
            List<int> groupsid = new List<int>(); ;
            try
            {
                WS.PagedDeviceList page = cc.GetAllDevices(null, -1);
                devices = new WS.Device[page.TotalItemCount];
                devices = page.Items;

                if (devices != null)
                {
                    for (int i = 0; i < devices.Length; i++)
                    {
                        if (devices[i].ReportedGroupId == id)
                        {
                            groupsid.Add(devices[i].Id);
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            return groupsid;
        }


        protected void redirectGroups(object sender, EventArgs e)
        {
            Response.Redirect("Grupos.aspx");
        }

        protected void getGroups()
        {

            try
            {
                // Begin using the client.
                WS.PagedGroupList page = cc.GetGroups(null, -1);
                Listg = new WS.Group[page.TotalItemCount];
                Listg = page.Items;
                // prueba.Text = page.TotalItemCount.ToString();
            }
            catch (Exception e)
            {
                // prueba.Text = e.Message;
            }
        }

        
    }





}