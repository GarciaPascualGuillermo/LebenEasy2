﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Menu.aspx.cs" Inherits="LebenE.WebForm1" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="widtd=device-widtd, initial-scale=1.0">

    <!-- Estilos css y estilos DataTables -->
    <link rel="stylesheet" href="css/nav-bar.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/general.css">
    <link rel="stylesheet" href="css/table-players.css">
    <link rel="stylesheet" href="css/descarga-logs.css">
    <link rel="stylesheet" href="css/modal-usuario.css">
    <link rel="stylesheet" href="css/DataTables.css">
    <link rel="stylesheet" href="css/DataTables.Select.min.css">

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">

    <title>Bienvenido a LebenEasyPlayer</title>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
</head>

<body class="background-dark">

    <form id="f_menuPage" runat="server">
    <!-- Barra de navegación -->
    <nav>
        <input type="checkbox" id="check">
        <label for="check" class="checkbtn">
            <i class="fas fa-bars"></i>
        </label>

        <img class="img-logo" src="Img/leben_simbol.png" alt="">
        <label class="logo">
            LebenEasyPlayer</label>
        <ul>
            <li><asp:LinkButton runat="server" CssClass="hyperlink active" NavigateUrl="#" ID="home"  Text="Inicio"/></li>
            <!-- Codigo para ejecutar funciones Javascript desde la propiedad href de los elementos 'hyperlink' -->
            <li><asp:HyperLink runat="server" CssClass="hyperlink" NavigateUrl="javascript:show_propiedadesUser();" ID="user_prop_hyper" Text="Mi perfil"/></li>
            <!-- //Fin de código -->
            <li><asp:LinkButton runat="server" CssClass="hyperlink" NavigateUrl="#" onclick="redirectGroups" ID="admin_grupos_hyper" Text="Reporte por grupos"/></li>
            <li><asp:LinkButton ID="link_button" CssClass="hyperlink" runat="server" onclick="Logout"  Text="Cerrar Sesión" /></li>
        </ul>
    </nav>


    <!-- Barra lateral de grupos -->
    <div class="sidebar" id="contenedor"  runat="server" >
        <asp:Label runat="server" ID="name" text="" CssClass="hyperlink"></asp:Label>
        <h1>Grupos</h1>
        
        <asp:PlaceHolder id="gru" runat="server"></asp:PlaceHolder>    
        <asp:Label runat="server" ID="t1" text="" CssClass="hyperlink"></asp:Label>
        <asp:Label runat="server" ID="t2" text="" CssClass="hyperlink"></asp:Label>
        <asp:Label runat="server" ID="t3" text="" CssClass="hyperlink"></asp:Label>
        <asp:Label runat="server" ID="t4" text="" CssClass="hyperlink"></asp:Label>
        <asp:Label runat="server" ID="t5" text="" CssClass="hyperlink"></asp:Label>
        <asp:Label runat="server" ID="t6" text="" CssClass="hyperlink"></asp:Label>
        <asp:Label runat="server" ID="t7" text="" CssClass="hyperlink"></asp:Label>
        <asp:Label runat="server" ID="t8" text="" CssClass="hyperlink"></asp:Label>
    </div>


    <!-- Botón para generar reportes -->
    <div class="col-buttons">
               <label id="generate_report" class="button-inverse" onclick="show_descargar_logs_modal();">Generar Reporte</label>

       </div>

    <div class="table-container">
        <!-- Tabla para players -->
         <asp:PlaceHolder id="tabla2" runat="server"></asp:PlaceHolder>
        
        

    </div>
       


<!-- CODIGO PARA DESCARGAR EL LOG ESPECIFICADO -->
    <!-- The Modal-log -->
    <div id="myModal-log" class="modal-log">

        <!-- Modal-log content -->
        <div class="modal-log-content">
            <div class="modal-log-header">
                <label id="myClose-log" class="close-log" onclick="hide_descargar_logs_modal();"> &times; </label>
            </div>
            <div class="modal-log-body">
                <h2>Propiedades de su archivo log</h2>
                <br>
                
                    <b>Seleccione su tipo de Reporte</b>
                    <br>
                    <p><asp:DropDownList ID="report_period" CssClass="dropdownlist-dark pickers" runat="server"></asp:DropDownList></p><input type="date" runat="server" name="inicio_fecha" id="dt_inicio" class="pickers">
                    <br>
                    <br>
                    <b>Seleccione su formato del archivo log</b>
                    <br>
                    <br>
                    <input type="radio" runat="server" name="rd-formato" id="rd_json" value="json" class="checkers" checked/> <label for="rd_json">JSON(.json)</label>
                    <input type="radio" runat="server" name="rd-formato" id="rd_pdf" value="pdf" class="checkers"/><label for="rd_pdf">PDF(.pdf)</label>
                    <input type="radio" runat="server" name="rd-formato" id="rd_csv" value="csv" class="checkers"/><label for="rd_csv">Excel(.csv)</label>
                    <!-- <input type="radio" name="rd-formato" id="rd-xlsx" value="xlsx" ><label for="rd-xlsx">Excel(.xlsx)</label></input> -->
                    <br>
                    <br>
                    <br>
                <button name="download" id="HTMLbtn_downloadLog" class="button-inverse" runat="server">Descargar log</button>
                
            </div>
        </div>

    </div>
    <!-- //FIN DE CODIGO PARA DESCARGAR EL LOG ESPECIFICADO -->


    <!-- MODAL PARA VER LAS PROPIEDADES DE LA SESION -->

    <div class="modal-user" id="modal_propiedades_user" runat="server">
        <div class="modal-user-content">
            <div class="modal-user-body">
                <h2>Propiedades del Usuario</h2>
                    <br />
                    <p><b>User ID:</b> <asp:Label runat="server" ID="lb_userID">sdgsd</asp:Label></p>
                    <br />
                    <p><b>User Name:</b> <asp:Label runat="server" ID="lb_username">sdgsd</asp:Label></p>
                    <br />
                    <p><b>User LastName:</b> <asp:Label runat="server" ID="lb_userlastname">sdgsd</asp:Label></p>
                    <br />  
                    <p><b>User Email:</b> <asp:Label runat="server" ID="lb_userEmail">sdgsd</asp:Label></p>
                    <br />
                    <p><b>Role:</b> <asp:Label runat="server" ID="lb_role">sdgsd</asp:Label></p>
                    <br />
                    <p><b>Descripción:</b> <asp:Label runat="server" ID="lb_descrip">sdgsd</asp:Label></p>
                    <br />
                    <button class="button-inverse" id="x" onclick="hide_propiedadesUser();">Aceptar</button>
            </div>
        </div>
         
    </div>

   
    <!-- //FIN DE MODAL PARA VER LAS PROPIEDADES DE LA SESION -->


    <!-- Scripts -->
    <script type="text/javascript">
        function checkMe() {
            PageMethods.Logout();
        }
        function onComplete(result, response, content) {
            alert(result);
        }
    </script>


    <!-- JS, Popper.js, and jQuery -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js">
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>


    <!-- JS de DataTables -->
    <script type="text/javascript" src="js/table-players.js"></script>
    <script type="text/javascript" src="js/descarga-logs.js"></script>
    <script type="text/javascript" src="js/modal-usuario.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>

        </form>
</body>


</html>